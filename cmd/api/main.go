package main

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/aydinmuzaffer/sofram/internal/db"
	"github.com/aydinmuzaffer/sofram/internal/handlers"
	"github.com/aydinmuzaffer/sofram/internal/repos"
	services "github.com/aydinmuzaffer/sofram/internal/services"

	_ "github.com/aydinmuzaffer/sofram/docs"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v7"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	log "github.com/sirupsen/logrus"
)

var client *redis.Client

func Run() error {
	log.SetFormatter(&log.JSONFormatter{})
	log.Info("Setting Up Our APP")

	soframCon, err := db.NewSoframConnection()
	if err != nil {
		log.Error("failed to setup connection to the database")
		return err
	}
	defer soframCon.Close()

	if err := soframCon.Ping(context.Background()); err != nil {
		log.Error("failed to setup database")
		return err
	}

	if err := soframCon.MigrateDB(); err != nil {
		log.Error("failed to migrate database")
		return err
	}

	seedRepository := repos.NewSeedRepository(soframCon)
	if err := seedRepository.SeedDB(); err != nil {
		log.Error("failed to seed database")
		return err
	}

	//Initializing redis
	dsn := os.Getenv("REDIS_DSN")
	client := redis.NewClient(&redis.Options{
		Addr:     dsn,
		Password: "",
		DB:       0,
	})
	if err := client.Ping().Err(); err != nil {
		log.Error("failed to ping redis")
		return err
	}

	log.Info("setting up our handler")

	roleRepo := repos.NewRoleRepository()
	roleService := services.NewRoleService(roleRepo)
	roleHandler := handlers.NewRoleHandler(roleService)

	userStatusRepo := repos.NewUserStatusRepository()
	userStatusService := services.NewUserStatusService(userStatusRepo)
	userStatusHandler := handlers.NewUserStatusHandler(userStatusService)

	orderStatusRepo := repos.NewOrderStatusRepository()
	orderStatusService := services.NewOrderStatusService(orderStatusRepo)
	orderStatusHandler := handlers.NewOrderStatusHandler(orderStatusService)

	paymentMethodRepo := repos.NewPaymentMethodRepository()
	paymentMethodService := services.NewPaymentMethodService(paymentMethodRepo)
	paymentMethodHandler := handlers.NewPaymentMethodHandler(paymentMethodService)

	userRepo := repos.NewUserRepository(soframCon)
	authCache := services.NewAuthCacheService(client)
	jwtService := services.NewJwtService()
	authService := services.NewAuthService(userRepo, authCache, jwtService)
	authHandler := handlers.NewAuthHandler(authService)

	healthCheckHandler := handlers.NewPostgresHealthCheckHandler(soframCon)

	router := gin.Default()

	v1 := router.Group("/api/v1")
	{
		roles := v1.Group("/roles")
		{
			roles.GET("", roleHandler.Get)
		}

		userStatus := v1.Group("/user-statuses")
		{
			userStatus.GET("", userStatusHandler.Get)
		}

		orderStatus := v1.Group("/order-statuses")
		{
			orderStatus.GET("", orderStatusHandler.Get)
		}

		paymentMethod := v1.Group("/payment-methods")
		{
			paymentMethod.GET("", paymentMethodHandler.Get)
		}

		auth := v1.Group("/auth")
		{
			auth.POST("/register", authHandler.Register)
			auth.GET("/confirm", authHandler.ConfirmAccount)
			auth.POST("/login", authHandler.Login)
		}
	}
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	// Routes
	router.GET("/health", healthCheckHandler.Live)
	router.GET("/health/live", healthCheckHandler.Live)
	router.GET("/health/ready", healthCheckHandler.Ready)

	server := &http.Server{
		Addr:    ":8080",
		Handler: router,
	}

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := server.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Fatal("Server ListenAndServe error")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 2)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Info("Shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown")
	}

	log.Info("Server exiting.")

	return nil
}

// @title Sofram API
// @version 1.0
// @BasePath  /api/v1
func main() {
	if err := Run(); err != nil {
		log.Error(err)
		log.Fatal("Error starting up our REST API")
	}
}
