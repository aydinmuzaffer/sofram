include .env

.EXPORT_ALL_VARIABLES:

.PHONY: install_deps build swag upload tidy

install_deps:
	go mod download
	go install github.com/swaggo/swag/cmd/swag@v1.7.4

build:
	swag init -g ./cmd/api/main.go
	go build -o ./$(GOCONFIG_SERVICENAME) cmd/api/main.go

swag:
	# export PATH=$(go env GOPATH)/bin:$PATH
	swag init -g ./cmd/api/main.go

upload:
	docker-compose up --build

tidy:
	go mod tidy