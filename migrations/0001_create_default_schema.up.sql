begin;

create table if not exists "role" (
id uuid primary key not null unique,
"name" text,
"description" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists user_status (
id uuid primary key not null unique,
"name" text,
"description" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "user" (
id uuid primary key not null unique,
"name" text,
surname text,
email text,
phone text,
"password" text,
role_id uuid,
user_status_id uuid,
is_vendor boolean,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists neighbourhood (
id uuid primary key not null unique,
"name" text,
county_id uuid,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists city (
id uuid primary key not null unique,
"name" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists county (
id uuid primary key not null unique,
"name" text,
city_id uuid,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "address" (
id uuid primary key not null unique,
"name" text,
city_id uuid,
county_id uuid,
neighbourhood_id uuid,
"description" text,
"user_id" uuid,
is_active boolean,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp    
);

create table if not exists order_item (
id uuid primary key not null unique,
order_id uuid,
vendor_product_id uuid,
unit_price numeric
);

create table if not exists order_status (
id uuid primary key not null unique,
"name" text,
"description" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists payment_method (
id uuid primary key not null unique,
"name" text,
"description" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists product_category (
id uuid primary key not null unique,
"name" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists vendor_payment_method (
vendor_id uuid,
payment_method_id uuid,
is_active boolean,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp,
PRIMARY KEY(vendor_id, payment_method_id)
);

create table if not exists "order" (
id uuid primary key not null unique,
"user_id" int,
notes text,
vendor_payment_method_id uuid,
order_status_id uuid,
estimated_delivery_time time,
address_id uuid,
total numeric,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp     
);

create table if not exists vendor_product_category (
id uuid primary key not null unique,
vendor_id uuid,
"name" text,
product_category_id uuid,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists vendor_product (
id uuid primary key not null unique,
vendor_product_category_id uuid,
title text,
"description" text,
unit_price numeric,
thumbnail_img_path text,
imgage_path text,
is_active boolean,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp    
);

create table if not exists vendor (
"user_id" uuid primary key not null unique,
"name" text,
about text,
opening_hour time,
closing_hour time,
opening_day  int,
closing_day  int,
is_open      boolean,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);
commit;
