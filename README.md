# Sofram API
This is a monolith api for a simple food ordering service. It includes features ranging from authentication to creating menu items by vendors and leting customers giving orders. It is still under developement by the way. So far only a few endpoints are developed related to authentication and genereal code infrastruce is designed and applied.

## Tech

Sofram-Api uses a number of AWS cloud techs to work properly:

- [S3] - Aws S3 Bucket; used for storing images of menu items (not implemented yet)
- [LocalStack] - Cloud emulator for AWS services, used to test various AWS services locally
- [Docker] Used for containerization of the api
- [docker-compose] - Used for to be able to run multiple services locally like postgres, pgAdmin and api itself.
- [Gorm] - Database ORM of choice
- [golang-migrate] - A golang library for database migrations on startup through sql scripts
- [Make] - For easily organizing code compilation
- [Swago] - A golang swagger library to server api endpoint documentation
- [Golang] - The development languge of choice.

## Installation

Api requires [Golang](https://go.dev/) v1.18+ to run.

Clone the api somewhere in your computer.

```sh
cd sofram
make upload
```

Now browse http://localhost:8080/swagger/index.html and you should see the functining endpoints.

To stop the service and deleting volumes 
```sh
docker-compose down -v
```


