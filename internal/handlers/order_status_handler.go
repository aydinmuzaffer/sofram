package handlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram/internal/services"

	"github.com/gin-gonic/gin"
)

type OrderStatusHandler interface {
	Get(c *gin.Context)
}

type orderStatusHandler struct {
	OrderStatusService services.OrderStatusService
}

func NewOrderStatusHandler(orderStatusService services.OrderStatusService) OrderStatusHandler {
	return &orderStatusHandler{OrderStatusService: orderStatusService}
}

// GetOrderStatuses godoc
// @ID get-order-statuses
// @Summary Get Order Statuses
// @Description  This endpoint serves for getting all the Order statuses
// @Tags	order-statuses
// @Accept  json
// @Produce  json
// @Success      200  {array}  ResponseModel{data=services.OrderStatusGetModel}
// @Failure 	 422  {object} ResponseModel
// @Router /order-statuses [get]
func (s *orderStatusHandler) Get(c *gin.Context) {

	status, err := s.OrderStatusService.Get(c)

	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}
	res := ResponseModel{
		Data: status,
	}

	c.JSON(http.StatusOK, res)
}
