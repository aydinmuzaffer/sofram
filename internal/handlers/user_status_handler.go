package handlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram/internal/services"

	"github.com/gin-gonic/gin"
)

type UserStatusHandler interface {
	Get(c *gin.Context)
}

type userStatusHandler struct {
	userStatusService services.UserStatusService
}

func NewUserStatusHandler(userStatusService services.UserStatusService) UserStatusHandler {
	return &userStatusHandler{userStatusService: userStatusService}
}

// GetUserStatuses godoc
// @ID get-user-statuses
// @Summary Get User Statuses
// @Description  This endpoint serves for getting all the user statuses
// @Tags	user-statuses
// @Accept  json
// @Produce  json
// @Success      200  {array}  ResponseModel{data=services.UserStatusGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /user-statuses [get]
func (s *userStatusHandler) Get(c *gin.Context) {

	status, err := s.userStatusService.Get(c)

	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}
	res := ResponseModel{
		Data: status,
	}
	c.JSON(http.StatusOK, res)
}
