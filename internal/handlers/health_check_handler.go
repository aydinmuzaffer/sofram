package handlers

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/aydinmuzaffer/sofram/internal/db"
	"github.com/gin-gonic/gin"
)

const pingTimeout = 3

type HealthCheckHandler interface {
	Live(*gin.Context)
	Ready(*gin.Context)
}

type postgresHealthCheckHandler struct {
	sc *db.SoframConnection
}

func NewPostgresHealthCheckHandler(sc *db.SoframConnection) HealthCheckHandler {
	return &postgresHealthCheckHandler{sc: sc}
}

func (h *postgresHealthCheckHandler) Live(c *gin.Context) {
	c.Status(http.StatusNoContent)
}

func (h *postgresHealthCheckHandler) Ready(c *gin.Context) {

	ctxPing, cancelPing := context.WithTimeout(c.Request.Context(), time.Duration(pingTimeout)*time.Second)
	defer cancelPing()

	err := h.sc.Ping(ctxPing)
	if err != nil {
		c.JSON(http.StatusRequestTimeout, fmt.Errorf("Db ping timeout with %s", err.Error()))
	}

	c.Status(http.StatusNoContent)
}
