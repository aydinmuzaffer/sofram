package handlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram/internal/services"

	"github.com/gin-gonic/gin"
)

type RoleHandler interface {
	Get(c *gin.Context)
}

type roleHandler struct {
	roleService services.RoleService
}

func NewRoleHandler(roleService services.RoleService) RoleHandler {
	return &roleHandler{roleService: roleService}
}

// GetRoles godoc
// @ID get-roles
// @Summary Get Roles
// @Description  This endpoint serves for getting all the roles
// @Tags	roles
// @Accept  json
// @Produce  json
// @Success      200  {array}  ResponseModel{data=services.RoleGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /roles [get]
func (r *roleHandler) Get(c *gin.Context) {

	role, err := r.roleService.Get(c)

	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}
	res := ResponseModel{
		Data: role,
	}
	c.JSON(http.StatusOK, res)
}
