package handlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram/internal/services"

	"github.com/gin-gonic/gin"
)

type PaymentMethodHandler interface {
	Get(c *gin.Context)
}

type paymentMethodHandler struct {
	PaymentMethodService services.PaymentMethodService
}

func NewPaymentMethodHandler(paymentMethodService services.PaymentMethodService) PaymentMethodHandler {
	return &paymentMethodHandler{PaymentMethodService: paymentMethodService}
}

// GetPaymentMethods godoc
// @ID get-payment-methods
// @Summary Get Payment Methods
// @Description  This endpoint serves for getting all the payment methods
// @Tags	payment-methods
// @Accept  json
// @Produce  json
// @Success      200  {array}  ResponseModel{data=services.PaymentMethodGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /payment-methods [get]
func (s *paymentMethodHandler) Get(c *gin.Context) {

	status, err := s.PaymentMethodService.Get(c)

	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}

	res := ResponseModel{
		Data: status,
	}

	c.JSON(http.StatusOK, res)
}
