package handlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram/internal/services"
	"github.com/gin-gonic/gin"
)

type authHandler struct {
	Service services.AuthService
}

type AuthHandler interface {
	Register(c *gin.Context)
	ConfirmAccount(c *gin.Context)
	Login(c *gin.Context)
	Logout(c *gin.Context)
	Refresh(c *gin.Context)
}

func NewAuthHandler(service services.AuthService) AuthHandler {
	return &authHandler{
		Service: service,
	}
}

// RegisterUser godocs
// @ID register-user
// @Summary      Register a user
// @Description  This endpoint serves for creating new user
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param data body services.UserRegisterModel true "The input UserRegisterModel struct"
// @Success      200
// @Failure 	 500  {object}  ResponseModel
// @Router /auth/register [post]
func (h *authHandler) Register(c *gin.Context) {
	// Validate input
	var input services.UserRegisterModel
	if err := c.ShouldBindJSON(&input); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusBadRequest, res)
	}

	err := h.Service.Register(c, input)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}

	c.Status(http.StatusOK)
}

// ConfirmAccount godocs
// @ID confirm-account
// @Summary      Confirm account
// @Description  This endpoint serves for confirming new user
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param data body services.UserConfirmAccountModel true "The input UserConfirmAccountModel struct"
// @Success      200
// @Failure 	 500  {object}  ResponseModel
// @Router /auth/confirm [post]
func (uh *authHandler) ConfirmAccount(c *gin.Context) {
	// Validate input
	var input services.UserConfirmAccountModel
	if err := c.ShouldBindJSON(&input); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusBadRequest, res)
	}

	err := uh.Service.ConfirmAccount(c, input)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}

	c.Status(http.StatusOK)
}

// Login godocs
// @ID login-user
// @Summary      Login a user
// @Description  This endpoint serves for loging a user
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param data body services.UserLoginModel true "The input UserLoginModel struct"
// @Success      200 {object}  ResponseModel{data=services.Token}
// @Failure 	 500  {object}  ResponseModel
// @Router /auth/login [post]
func (uh *authHandler) Login(c *gin.Context) {
	// Validate input
	var input services.UserLoginModel
	if err := c.ShouldBindJSON(&input); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusBadRequest, res)
	}

	token, err := uh.Service.Login(c, input)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}

	res := ResponseModel{
		Data: token,
	}
	c.JSON(http.StatusOK, res)
}

// RefreshToken godocs
// @ID refresh-token
// @Summary      Refreshes auth token
// @Description  This endpoint serves for refreshing auth token
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param data body services.TokenRefrehModel true "The input TokenRefrehModel struct"
// @Success      200 {object}  ResponseModel{data=services.Token}
// @Failure 	 500  {object}  ResponseModel
// @Router /auth/refresh [post]
func (uh *authHandler) Refresh(c *gin.Context) {
	// Validate input
	var input services.TokenRefrehModel
	if err := c.ShouldBindJSON(&input); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusBadRequest, res)
	}

	token, err := uh.Service.RefreshToken(c, input)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}

	res := ResponseModel{
		Data: token,
	}
	c.JSON(http.StatusOK, res)
}

// Logout godocs
// @ID logout-user
// @Summary      Logout a user
// @Description  This endpoint serves for logging out a user
// @Tags         auth
// @Accept       json
// @Produce      json
// @Success      200
// @Failure 	 500  {object}  ResponseModel
// @Router /auth/logout [get]
func (uh *authHandler) Logout(c *gin.Context) {
	if err := uh.Service.Logout(c.Request.Context(), c.Request); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}
	c.Status(http.StatusOK)
}
