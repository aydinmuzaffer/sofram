package repos

import (
	"context"
	"errors"

	"github.com/aydinmuzaffer/sofram/internal/domain"
)

type roleRepository struct{}

func NewRoleRepository() domain.RoleRepository {
	return &roleRepository{}
}

func (r *roleRepository) Get(ctx context.Context) ([]domain.Role, error) {
	var roles []domain.Role

	roleEnum := domain.GetEnumList[any, *domain.Role](domain.RoleEnum)

	if len(roleEnum) == 0 {
		return roles, errors.New("empty roles")
	}

	for _, v := range roleEnum {
		roles = append(roles, *v)
	}

	return roles, nil
}
