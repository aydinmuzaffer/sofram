package repos

import (
	"context"
	"errors"

	"github.com/aydinmuzaffer/sofram/internal/domain"
)

type paymentMethodRepository struct{}

func NewPaymentMethodRepository() domain.PaymentMethodRepository {
	return &paymentMethodRepository{}
}

func (r *paymentMethodRepository) Get(ctx context.Context) ([]domain.PaymentMethod, error) {
	var statuses []domain.PaymentMethod

	PaymentMethodEnum := domain.GetEnumList[any, *domain.PaymentMethod](domain.PaymentMethodEnum)

	if len(PaymentMethodEnum) == 0 {
		return statuses, errors.New("empty payment methods")
	}

	for _, v := range PaymentMethodEnum {
		statuses = append(statuses, *v)
	}

	return statuses, nil
}
