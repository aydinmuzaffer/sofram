package repos

import (
	"errors"
	"fmt"

	"github.com/aydinmuzaffer/sofram/internal/db"
	"github.com/aydinmuzaffer/sofram/internal/domain"

	//"github.com/jinzhu/gorm"
	"gorm.io/gorm"

	log "github.com/sirupsen/logrus"
)

type SeedRepository interface {
	SeedDB() error
}

type seedRepository struct {
	Con *db.SoframConnection
}

func NewSeedRepository(con *db.SoframConnection) SeedRepository {
	return &seedRepository{
		Con: con,
	}
}

func (sr *seedRepository) SeedDB() error {
	log.Info("seeding our database")

	sr.Con.Db.Debug().Transaction(func(tx *gorm.DB) error {
		// do some database operations in the transaction (use 'tx' from this point, not 'db')
		if err := checkDefaultRoles(tx); err != nil {
			return err
		}

		if err := checkDefaultUserStatuses(tx); err != nil {
			return err
		}

		if err := cehckDefaultOrderStatuses(tx); err != nil {
			return err
		}

		if err := checkDefaultPaymentMethods(tx); err != nil {
			return err
		}

		// return nil will commit the whole transaction
		return nil
	})

	log.Info("successfully seeded the database")

	return nil
}

func checkDefaultRoles(tx *gorm.DB) error {
	admin := domain.RoleEnum.Admin
	if err := setRole(tx, admin); err != nil {
		return err
	}

	vendor := domain.RoleEnum.Vendor
	if err := setRole(tx, vendor); err != nil {
		return err
	}

	backoffice := domain.RoleEnum.BackOffice
	if err := setRole(tx, backoffice); err != nil {
		return err
	}

	enduser := domain.RoleEnum.EndUser
	if err := setRole(tx, enduser); err != nil {
		return err
	}

	return nil
}

func setRole(tx *gorm.DB, r *domain.Role) error {
	if !rowExists(tx, "SELECT 1 FROM role WHERE name = ?", r.Name) {
		if err := createRow(tx, r); err != nil {
			return err
		}
		return nil
	} else {
		existing := &domain.Role{}
		tx.Where("name = ?", r.Name).First(existing)
		r = existing
		return nil
	}
}

func checkDefaultUserStatuses(tx *gorm.DB) error {
	registered := domain.UserStatusEnum.Registered
	if err := setUserStatus(tx, registered); err != nil {
		return err
	}

	waitingForApproval := domain.UserStatusEnum.WaitingForApproval
	if err := setUserStatus(tx, waitingForApproval); err != nil {
		return err
	}

	confirmed := domain.UserStatusEnum.Confirmed
	if err := setUserStatus(tx, confirmed); err != nil {
		return err
	}

	rejected := domain.UserStatusEnum.Rejected
	if err := setUserStatus(tx, rejected); err != nil {
		return err
	}

	return nil
}

func setUserStatus(tx *gorm.DB, s *domain.UserStatus) error {
	if !rowExists(tx, "SELECT 1 FROM user_status WHERE name = ?", s.Name) {
		if err := createRow(tx, s); err != nil {
			return err
		}
		return nil
	} else {
		existing := &domain.UserStatus{}
		tx.Where("name = ?", s.Name).First(existing)
		s = existing
		return nil
	}
}

func checkDefaultPaymentMethods(tx *gorm.DB) error {
	cash := domain.PaymentMethodEnum.Cash
	if err := setPaymentMethod(tx, cash); err != nil {
		return err
	}

	creditCardOnDelivery := domain.PaymentMethodEnum.CreditCardOnDelivery
	if err := setPaymentMethod(tx, creditCardOnDelivery); err != nil {
		return err
	}

	ticketCard := domain.PaymentMethodEnum.TicketCard
	if err := setPaymentMethod(tx, ticketCard); err != nil {
		return err
	}

	sodexoCard := domain.PaymentMethodEnum.SodexoCard
	if err := setPaymentMethod(tx, sodexoCard); err != nil {
		return err
	}

	return nil
}

func setPaymentMethod(tx *gorm.DB, p *domain.PaymentMethod) error {
	if !rowExists(tx, "SELECT 1 FROM payment_method WHERE name = ?", p.Name) {
		if err := createRow(tx, p); err != nil {
			return err
		}
		return nil
	} else {
		existing := &domain.PaymentMethod{}
		tx.Where("name = ?", p.Name).First(existing)
		p = existing
		return nil
	}
}

func cehckDefaultOrderStatuses(tx *gorm.DB) error {

	created := domain.OrderStatusEnum.Created
	if err := setOrderStatus(tx, created); err != nil {
		return err
	}

	approved := domain.OrderStatusEnum.Approved
	if err := setOrderStatus(tx, approved); err != nil {
		return err
	}

	rejected := domain.OrderStatusEnum.Rejected
	if err := setOrderStatus(tx, rejected); err != nil {
		return err
	}

	onDelivery := domain.OrderStatusEnum.OnDelivery
	if err := setOrderStatus(tx, onDelivery); err != nil {
		return err
	}

	delivered := domain.OrderStatusEnum.Delivered
	if err := setOrderStatus(tx, delivered); err != nil {
		return err
	}

	return nil
}

func setOrderStatus(tx *gorm.DB, s *domain.OrderStatus) error {
	if !rowExists(tx, "SELECT 1 FROM order_status WHERE name = ?", s.Name) {
		if err := createRow(tx, s); err != nil {
			return err
		}
		return nil
	} else {
		existing := &domain.OrderStatus{}
		tx.Where("name = ?", s.Name).First(existing)
		s = existing
		return nil
	}
}

func rowExists(tx *gorm.DB, query string, args ...interface{}) bool {
	var exists bool
	query = fmt.Sprintf("SELECT exists (%s)", query)
	err := tx.Raw(query, args...).Row().Scan(&exists)
	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		log.Fatalf("error checking if row exists '%s' %v", args, err)
	}
	return exists
}

func createRow(tx *gorm.DB, value interface{}) error {
	if err := tx.Session(&gorm.Session{SkipHooks: true}).Debug().Create(value).Error; err != nil {
		return err
	}
	return nil
}
