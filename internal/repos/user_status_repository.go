package repos

import (
	"context"
	"errors"

	"github.com/aydinmuzaffer/sofram/internal/domain"
)

type userStatusRepository struct{}

func NewUserStatusRepository() domain.UserStatusRepository {
	return &userStatusRepository{}
}

func (r *userStatusRepository) Get(ctx context.Context) ([]domain.UserStatus, error) {
	var statuses []domain.UserStatus

	userStatusEnum := domain.GetEnumList[any, *domain.UserStatus](domain.UserStatusEnum)

	if len(userStatusEnum) == 0 {
		return statuses, errors.New("empty user statuses")
	}

	for _, v := range userStatusEnum {
		statuses = append(statuses, *v)
	}

	return statuses, nil
}
