package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram/internal/db"
	"github.com/aydinmuzaffer/sofram/internal/domain"
)

type userRepository struct {
	Con *db.SoframConnection
}

// Add implements domain.UserRepository
func (r *userRepository) Add(ctx context.Context, u *domain.User) error {
	if err := r.Con.Db.WithContext(ctx).Create(u).Error; err != nil {
		return err
	}

	return nil
}

// GetByEmail implements domain.UserRepository
func (r *userRepository) GetByEmail(ctx context.Context, email string) (*domain.User, error) {
	user := &domain.User{}
	if err := r.Con.Db.WithContext(ctx).Where(&domain.User{Email: email}).First(&user).Error; err != nil {
		return user, err
	}
	return user, nil
}

// Login implements domain.UserRepository
func (r *userRepository) Login(ctx context.Context, email string, password string) (*domain.User, error) {
	user := &domain.User{}
	if err := r.Con.Db.WithContext(ctx).Where(&domain.User{Email: email, Password: password}).First(&user).Error; err != nil {
		return user, err
	}
	return user, nil
}

// Update implements domain.UserRepository
func (r *userRepository) Update(ctx context.Context, u *domain.User) error {
	if err := r.Con.Db.WithContext(ctx).Save(u).Error; err != nil {
		return err
	}
	return nil
}

func NewUserRepository(con *db.SoframConnection) domain.UserRepository {
	return &userRepository{
		Con: con,
	}
}
