package repos

import (
	"context"
	"errors"

	"github.com/aydinmuzaffer/sofram/internal/domain"
)

type orderStatusRepository struct{}

func NewOrderStatusRepository() domain.OrderStatusRepository {
	return &orderStatusRepository{}
}

func (r *orderStatusRepository) Get(ctx context.Context) ([]domain.OrderStatus, error) {
	var statuses []domain.OrderStatus

	orderStatusEnum := domain.GetEnumList[any, *domain.OrderStatus](domain.OrderStatusEnum)

	if len(orderStatusEnum) == 0 {
		return statuses, errors.New("empty order statuses")
	}

	for _, v := range orderStatusEnum {
		statuses = append(statuses, *v)
	}

	return statuses, nil
}
