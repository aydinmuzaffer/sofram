package services

import (
	"context"
	"errors"
	"net/http"

	"github.com/aydinmuzaffer/sofram/internal/domain"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

type authService struct {
	Repo  domain.UserRepository
	Cache AuthCacheService
	Jwt   JwtService
}

type AuthService interface {
	Register(ctx context.Context, model UserRegisterModel) error
	ConfirmAccount(ctx context.Context, model UserConfirmAccountModel) error
	ForgotPassword(ctx context.Context, model ForgotPasswordModel) error
	ResetPassword(ctx context.Context, model ResetPasswordModel) error
	Login(ctx context.Context, model UserLoginModel) (Token, error)
	Logout(ctx context.Context, request *http.Request) error
	RefreshToken(ctx context.Context, model TokenRefrehModel) (Token, error)
}

func (s *authService) Register(ctx context.Context, model UserRegisterModel) error {
	var user = &domain.User{}
	user, err := s.Repo.GetByEmail(ctx, model.Email)
	if err == nil && user.Email != "" {
		log.Error("existing user", err)
		return errors.New("existing user")
	}

	user.Email = model.Email
	user.Name = model.Name
	user.Surname = model.Surname
	user.Phone = model.Phone
	user.RoleId = domain.RoleEnum.EndUser.ID
	user.UserStatusId = domain.UserStatusEnum.Registered.ID

	if hashedPassword, err := generatehashPassword(model.Password); err != nil {
		log.Error("error at registering user while password hashing", err)
		return errors.New("error at registering user")
	} else {
		user.Password = hashedPassword // Hash password from authentication_service
	}

	if err := s.Repo.Add(ctx, user); err != nil {
		return err
	}

	accountCofirmationToken, err := s.Jwt.CreateAccountConfirmationToken(user.Email)
	if err != nil {
		log.Error("error at registering user while creating confirmation token", err)
		return errors.New("error at registering user")
	}

	//TODO: send account confirmation notification calling notification api recursively until it succeeds, do it in  a go routine
	//after that call aws lamda function to update user status to waitingForApproval
	log.Info(accountCofirmationToken)
	return nil
}

func (s *authService) ConfirmAccount(ctx context.Context, model UserConfirmAccountModel) error {

	email, err := s.Jwt.ValidateAccountConfirmationToken(model.Token)
	if err != nil {
		log.Error("error at validating user confirmation token", err)
		return errors.New("invalid token")
	}

	user, err := s.Repo.GetByEmail(ctx, email)
	if err != nil {
		log.Error("user nof found at validating user confirmation token", err)
		return errors.New("invalid token")
	}

	if user.UserStatusId != domain.UserStatusEnum.WaitingForApproval.ID {
		log.Error("user status not in WaitingForApproval", err)
		return errors.New("invalid token")
	}

	user.UserStatusId = domain.UserStatusEnum.Confirmed.ID
	if err := s.Repo.Update(ctx, user); err != nil {
		log.Error("user nof found at validating user confirmation token", err)
		return errors.New("invalid token")
	}
	return nil
}

func (s *authService) ForgotPassword(ctx context.Context, model ForgotPasswordModel) error {
	user, err := s.Repo.GetByEmail(ctx, model.Email)
	if err != nil {
		log.Error("user nof found at fogot password", err)
		return nil
	}

	if user.UserStatusId != domain.UserStatusEnum.Confirmed.ID {
		log.Error("user status not Confirmed at forgot password", err)
		return nil
	}

	//TODO: send forgot mail notification calling notification api recursively until it succeeds, do it in  a go routine
	return nil
}

func (s *authService) ResetPassword(ctx context.Context, model ResetPasswordModel) error {
	email, err := s.Jwt.ValidateAccountConfirmationToken(model.Token)
	if err != nil {
		log.Error("error at validating user fotgot password token", err)
		return errors.New("invalid token")
	}

	user, err := s.Repo.GetByEmail(ctx, email)
	if err != nil {
		log.Error("user nof found at validating user forgot password token", err)
		return errors.New("invalid token")
	}

	if user.UserStatusId != domain.UserStatusEnum.Confirmed.ID {
		log.Error("user status not in Confirmed at reset password", err)
		return errors.New("invalid token")
	}

	newPassword, err := generatehashPassword(model.NewPassword)
	if err != nil {
		log.Error("generate new password hash at reset password", err)
		return errors.New("invalid token")
	}

	user.Password = newPassword
	if err := s.Repo.Update(ctx, user); err != nil {
		log.Error("update user at reset password", err)
		return errors.New("invalid token")
	}
	return nil
}

func (s *authService) Login(ctx context.Context, model UserLoginModel) (Token, error) {
	var token = Token{}
	user, err := s.Repo.GetByEmail(ctx, model.Email)
	if err != nil {
		log.Error("user not found at login", err)
		return token, errors.New("invalid password or email")
	}

	if user.UserStatusId != domain.UserStatusEnum.Confirmed.ID {
		log.Error("user account not confirmed", err)
		return token, errors.New("invalid password or email")
	}

	password, err := generatehashPassword(model.Password)
	if err != nil {
		log.Error("password hash error at login", err)
		return token, errors.New("invalid password or email")
	}

	if password != user.Password {
		log.Error("password not mathc", err)
		return token, errors.New("invalid password or email")
	}

	tokenDetails, err := s.Jwt.CreateToken(user.Email)
	if err != nil {
		log.Error("login token creation", err)
		return token, errors.New("invalid password or email")
	}

	saveErr := s.Cache.CreateAuth(user.Email, tokenDetails)
	if saveErr != nil {
		log.Error("login token save", err)
		return token, errors.New("invalid password or email")
	}
	token.AccessToken = tokenDetails.AccessToken
	token.RefreshToken = tokenDetails.RefreshToken

	return token, nil
}

func (s *authService) Logout(ctx context.Context, request *http.Request) error {
	metadata, err := s.Jwt.ExtractTokenMetadata(request)
	if err != nil {
		log.Error("logout invalid token", err)
		return errors.New("invalid token")
	}
	delErr := s.Cache.DeleteTokens(metadata)
	if delErr != nil {
		log.Error(err)
		return errors.New("invalid token")
	}
	return nil
}

func (s *authService) RefreshToken(ctx context.Context, model TokenRefrehModel) (Token, error) {
	token := Token{}
	accessDetails, err := s.Jwt.VerifyRefreshToken(model.RefreshToken)
	if err != nil {
		return token, err
	}

	if _, err := s.Cache.DeleteAuth(accessDetails.AccessUuid); err != nil {
		return token, err
	}

	tokenDetails, err := s.Jwt.CreateToken(accessDetails.Email)
	if err != nil {
		log.Error("login token creation", err)
		return token, errors.New("invalid password or email")
	}

	saveErr := s.Cache.CreateAuth(accessDetails.Email, tokenDetails)
	if saveErr != nil {
		log.Error("login token save", err)
		return token, errors.New("invalid password or email")
	}
	token.AccessToken = tokenDetails.AccessToken
	token.RefreshToken = tokenDetails.RefreshToken

	return token, nil
}

func generatehashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func NewAuthService(repo domain.UserRepository, cache AuthCacheService, jwt JwtService) AuthService {
	return &authService{
		Repo:  repo,
		Cache: cache,
		Jwt:   jwt,
	}
}

type UserRegisterModel struct {
	Name     string
	Surname  string
	Email    string
	Phone    string
	Password string
}

type UserConfirmAccountModel struct {
	Token string
}

type UserLoginModel struct {
	Email    string
	Password string
}

type ForgotPasswordModel struct {
	Email string
}

type ResetPasswordModel struct {
	Token       string
	NewPassword string
}

type Token struct {
	AccessToken  string
	RefreshToken string
}

type TokenRefrehModel struct {
	RefreshToken string
}
