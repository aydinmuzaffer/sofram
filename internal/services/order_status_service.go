package services

import (
	"context"
	"fmt"

	"github.com/aydinmuzaffer/sofram/internal/domain"
)

type orderStatusService struct {
	repo domain.OrderStatusRepository
}

type OrderStatusService interface {
	Get(ctx context.Context) ([]OrderStatusGetModel, error)
}

func NewOrderStatusService(OrderStatusRepository domain.OrderStatusRepository) OrderStatusService {
	return &orderStatusService{
		repo: OrderStatusRepository,
	}
}

func (s *orderStatusService) Get(ctx context.Context) ([]OrderStatusGetModel, error) {

	orderStatus, err := s.repo.Get(ctx)
	if err != nil {
		return []OrderStatusGetModel{}, fmt.Errorf("order status getall: %w", err)
	}

	statusesToReturn := []OrderStatusGetModel{}

	for _, os := range orderStatus {
		status := OrderStatusGetModel{
			Name:        os.Name,
			Description: os.Description,
		}
		statusesToReturn = append(statusesToReturn, status)
	}

	return statusesToReturn, nil
}

type OrderStatusGetModel struct {
	Name        string
	Description string
}
