package services

import (
	"context"
	"fmt"

	"github.com/aydinmuzaffer/sofram/internal/domain"
)

type userStatusService struct {
	repo domain.UserStatusRepository
}

type UserStatusService interface {
	Get(ctx context.Context) ([]UserStatusGetModel, error)
}

func NewUserStatusService(userStatusRepository domain.UserStatusRepository) UserStatusService {
	return &userStatusService{
		repo: userStatusRepository,
	}
}

func (sr *userStatusService) Get(ctx context.Context) ([]UserStatusGetModel, error) {

	statuses, err := sr.repo.Get(ctx)
	if err != nil {
		return []UserStatusGetModel{}, fmt.Errorf("user status getall: %w", err)
	}

	statusesToReturn := []UserStatusGetModel{}

	for _, s := range statuses {
		status := UserStatusGetModel{
			Name:        s.Name,
			Description: s.Description,
		}
		statusesToReturn = append(statusesToReturn, status)
	}

	return statusesToReturn, nil
}

type UserStatusGetModel struct {
	Name        string
	Description string
}
