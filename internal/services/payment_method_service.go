package services

import (
	"context"
	"fmt"

	"github.com/aydinmuzaffer/sofram/internal/domain"
)

type paymentMethodService struct {
	repo domain.PaymentMethodRepository
}

type PaymentMethodService interface {
	Get(ctx context.Context) ([]PaymentMethodGetModel, error)
}

func NewPaymentMethodService(PaymentMethodRepository domain.PaymentMethodRepository) PaymentMethodService {
	return &paymentMethodService{
		repo: PaymentMethodRepository,
	}
}

func (s *paymentMethodService) Get(ctx context.Context) ([]PaymentMethodGetModel, error) {

	paymentMethods, err := s.repo.Get(ctx)
	if err != nil {
		return []PaymentMethodGetModel{}, fmt.Errorf("payment method getall: %w", err)
	}

	paymentMethodsToReturn := []PaymentMethodGetModel{}

	for _, pm := range paymentMethods {
		paymentMethod := PaymentMethodGetModel{
			Name:        pm.Name,
			Description: pm.Description,
		}
		paymentMethodsToReturn = append(paymentMethodsToReturn, paymentMethod)
	}

	return paymentMethodsToReturn, nil
}

type PaymentMethodGetModel struct {
	Name        string
	Description string
}
