package services

import (
	"errors"
	"fmt"
	"time"

	"github.com/go-redis/redis/v7"
)

type authCacheService struct {
	Client *redis.Client
}

type AuthCacheService interface {
	CreateAuth(email string, td *TokenDetails) error
	DeleteTokens(authD *AccessDetails) error
	DeleteAuth(givenUuid string) (int64, error)
}

func (s *authCacheService) CreateAuth(email string, td *TokenDetails) error {
	at := time.Unix(td.AtExpires, 0) //converting Unix to UTC(to Time object)
	rt := time.Unix(td.RtExpires, 0)
	now := time.Now()

	errAccess := s.Client.Set(td.AccessUuid, email, at.Sub(now)).Err()
	if errAccess != nil {
		return errAccess
	}
	errRefresh := s.Client.Set(td.RefreshUuid, email, rt.Sub(now)).Err()
	if errRefresh != nil {
		return errRefresh
	}
	return nil
}

func (s *authCacheService) FetchAuth(authD *AccessDetails) (string, error) {
	email, err := s.Client.Get(authD.AccessUuid).Result()
	if err != nil {
		return "", err
	}

	if authD.Email != email {
		return "", errors.New("unauthorized")
	}
	return email, nil
}

func (s *authCacheService) DeleteAuth(givenUuid string) (int64, error) {
	deleted, err := s.Client.Del(givenUuid).Result()
	if err != nil {
		return 0, err
	}
	return deleted, nil
}

func (s *authCacheService) DeleteTokens(authD *AccessDetails) error {
	//get the refresh uuid
	refreshUuid := fmt.Sprintf("%s++%d", authD.AccessUuid, authD.Email)
	//delete access token
	deletedAt, err := s.Client.Del(authD.AccessUuid).Result()
	if err != nil {
		return err
	}
	//delete refresh token
	deletedRt, err := s.Client.Del(refreshUuid).Result()
	if err != nil {
		return err
	}
	//When the record is deleted, the return value is 1
	if deletedAt != 1 || deletedRt != 1 {
		return errors.New("something went wrong deleting token")
	}
	return nil
}

func NewAuthCacheService(client *redis.Client) AuthCacheService {
	return &authCacheService{
		Client: client,
	}
}
