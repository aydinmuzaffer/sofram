package services

import (
	"context"
	"fmt"

	"github.com/aydinmuzaffer/sofram/internal/domain"
)

type roleService struct {
	repo domain.RoleRepository
}

type RoleService interface {
	Get(ctx context.Context) ([]RoleGetModel, error)
}

func NewRoleService(roleRepostiory domain.RoleRepository) RoleService {
	return &roleService{
		repo: roleRepostiory,
	}
}

func (rs *roleService) Get(ctx context.Context) ([]RoleGetModel, error) {

	roles, err := rs.repo.Get(ctx)
	if err != nil {
		return []RoleGetModel{}, fmt.Errorf("role getall: %w", err)
	}

	rolesToReturn := []RoleGetModel{}

	for _, r := range roles {
		role := RoleGetModel{
			Name:        r.Name,
			Description: r.Description,
		}
		rolesToReturn = append(rolesToReturn, role)
	}

	return rolesToReturn, nil
}

type RoleGetModel struct {
	Name        string
	Description string
}
