package domain

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type Vendor struct {
	UserId      uuid.UUID
	Name        string
	About       string
	OpeningHour time.Time
	ClosingHour time.Time
	OpeningDay  int
	ClosingDay  int
	IsOpen      bool
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
}

func (Vendor) TableName() string {
	return "vendor"
}
