package domain

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type VendorPaymentMethod struct {
	VendorId        uuid.UUID
	PaymentMethodId uuid.UUID
	IsActive        bool
	CreatedAt       time.Time
	UpdatedAt       time.Time
	DeletedAt       *time.Time
}

func (VendorPaymentMethod) TableName() string {
	return "vendor_payment_method"
}
