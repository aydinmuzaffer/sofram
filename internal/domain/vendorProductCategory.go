package domain

import (
	uuid "github.com/satori/go.uuid"
)

type VendorProductCategory struct {
	BaseEntitiy
	Name              string
	VendorId          uuid.UUID
	ProductCategoryId uuid.UUID
}

func (VendorProductCategory) TableName() string {
	return "vendor_product_category"
}
