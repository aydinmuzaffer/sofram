package domain

import uuid "github.com/satori/go.uuid"

type County struct {
	BaseEntitiy
	Name   string
	CityId uuid.UUID
}

func (County) TableName() string {
	return "county"
}
