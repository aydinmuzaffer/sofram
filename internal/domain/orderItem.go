package domain

import uuid "github.com/satori/go.uuid"

type OrderItem struct {
	BaseEntitiy
	OrderId         uuid.UUID
	VendorProductId uuid.UUID
	UnitPrice       float64
}

func (OrderItem) TableName() string {
	return "order_item"
}
