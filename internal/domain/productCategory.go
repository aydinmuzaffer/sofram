package domain

type ProductCategory struct {
	BaseEntitiy
	Name string
}

func (ProductCategory) TableName() string {
	return "product_category"
}
