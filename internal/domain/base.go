package domain

import (
	"time"

	// "github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type BaseEntitiy struct {
	ID        uuid.UUID
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

func (base *BaseEntitiy) BeforeCreate(scope *gorm.DB) error {
	base.ID = uuid.NewV4()
	return nil
}
