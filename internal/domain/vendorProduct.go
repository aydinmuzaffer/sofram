package domain

import (
	uuid "github.com/satori/go.uuid"
)

type VendorProduct struct {
	BaseEntitiy
	VendorProductCategoryId uuid.UUID
	Title                   string
	Description             string
	UnitPrice               float64
	ThumbnailImagePath      string
	ImagePath               string
	IsActive                bool
}

func (VendorProduct) TableName() string {
	return "vendor_product"
}
