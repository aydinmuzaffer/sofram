package domain

import (
	"context"
)

type UserStatus struct {
	Enumeration
}

func (UserStatus) TableName() string {
	return "user_status"
}

type UserStatusRepository interface {
	Get(ctx context.Context) ([]UserStatus, error)
}

func (p Enumeration) ConvertToUserStatus() *UserStatus {
	return &UserStatus{p}
}

var UserStatusEnum = struct {
	Registered         *UserStatus
	WaitingForApproval *UserStatus
	Confirmed          *UserStatus
	Rejected           *UserStatus
}{
	GetEnumeration("Registered", "Status when the user first registered").ConvertToUserStatus(),
	GetEnumeration("WaitingForApproval", "Status when the account confirmation link sent").ConvertToUserStatus(),
	GetEnumeration("Rejected", "Status when the user is rejected").ConvertToUserStatus(),
	GetEnumeration("Confirmed", "Status when the user confirm her regisration").ConvertToUserStatus(),
}
