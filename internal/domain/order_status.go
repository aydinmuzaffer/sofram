package domain

import (
	"context"
)

type OrderStatus struct {
	Enumeration
}

func (OrderStatus) TableName() string {
	return "order_status"
}

type OrderStatusRepository interface {
	Get(ctx context.Context) ([]OrderStatus, error)
}

func (p Enumeration) ConvertToOrderStatus() *OrderStatus {
	return &OrderStatus{p}
}

var OrderStatusEnum = struct {
	Created    *OrderStatus
	Approved   *OrderStatus
	Rejected   *OrderStatus
	OnDelivery *OrderStatus
	Delivered  *OrderStatus
}{
	GetEnumeration("Created", "Status when the order first placed").ConvertToOrderStatus(),
	GetEnumeration("Approved", "Status when the vendor approves the status").ConvertToOrderStatus(),
	GetEnumeration("Rejected", "Status when the vendor rejects the status").ConvertToOrderStatus(),
	GetEnumeration("OnDelivery", "Status when the rider takes the order from the vendor to deliver").ConvertToOrderStatus(),
	GetEnumeration("Delivered", "Status when the rider delivers the order and takes the payment").ConvertToOrderStatus(),
}
