package domain

import uuid "github.com/satori/go.uuid"

type Address struct {
	BaseEntitiy
	Name            string
	Description     string
	UserId          uuid.UUID
	CityId          uuid.UUID
	CountyId        uuid.UUID
	NeighbourhoodId uuid.UUID
	IsActive        bool
}

func (Address) TableName() string {
	return "address"
}
