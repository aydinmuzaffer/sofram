package domain

import (
	"context"
)

type Role struct {
	Enumeration
}

func (Role) TableName() string {
	return "role"
}

type RoleRepository interface {
	Get(ctx context.Context) ([]Role, error)
}

func (p Enumeration) ConvertToRole() *Role {
	return &Role{p}
}

var RoleEnum = struct {
	Admin      *Role
	Vendor     *Role
	BackOffice *Role
	EndUser    *Role
}{
	GetEnumeration("Admin", "Admin user has control over other users").ConvertToRole(),
	GetEnumeration("Vendor", "Vendors are shop owners").ConvertToRole(),
	GetEnumeration("BackOffice", "BackOffice users manage backoffice tasks").ConvertToRole(),
	GetEnumeration("EndUser", "EndUser is the customers who give orders").ConvertToRole(),
}
