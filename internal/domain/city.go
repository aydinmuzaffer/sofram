package domain

type City struct {
	BaseEntitiy
	Name string
}

func (City) TableName() string {
	return "city"
}
