package domain

import uuid "github.com/satori/go.uuid"

type Neighbourhood struct {
	BaseEntitiy
	Name     string
	CountyId uuid.UUID
}

func (Neighbourhood) TableName() string {
	return "neighbourhood"
}
