package domain

import (
	"reflect"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
)

type Enumeration struct {
	BaseEntitiy
	Name        string
	Description string
}

func GetEnumeration(name, description string) Enumeration {
	enum := Enumeration{}
	enum.ID = uuid.NewV4()
	enum.Name = name
	enum.Description = description
	return enum
}

func GetEnumList[T any, V any](t T) []V {
	v := reflect.ValueOf(t)
	var enums []V

	for i := 0; i < v.NumField(); i++ {
		data := v.Field(i).Interface()
		if enum, ok := data.(V); ok {
			enums = append(enums, enum)

		} else {
			log.Error("cant parse", data, reflect.TypeOf(data))
		}
	}

	return enums
}
