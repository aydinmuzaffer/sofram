package domain

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type User struct {
	BaseEntitiy
	Name         string
	Surname      string
	Email        string
	Phone        string
	Password     string
	RoleId       uuid.UUID
	UserStatusId uuid.UUID
	IsVendor     bool
}

func (User) TableName() string {
	return "user"
}

type UserRepository interface {
	Add(ctx context.Context, u *User) error
	GetByEmail(ctx context.Context, email string) (*User, error)
	Login(ctx context.Context, email string, password string) (*User, error)
	Update(ctx context.Context, u *User) error
}
