package domain

import (
	"context"
)

type PaymentMethod struct {
	Enumeration
}

func (PaymentMethod) TableName() string {
	return "payment_method"
}

type PaymentMethodRepository interface {
	Get(ctx context.Context) ([]PaymentMethod, error)
}

func (p Enumeration) ConvertToPaymentMethod() *PaymentMethod {
	return &PaymentMethod{p}
}

var PaymentMethodEnum = struct {
	Cash                 *PaymentMethod
	CreditCardOnDelivery *PaymentMethod
	TicketCard           *PaymentMethod
	SodexoCard           *PaymentMethod
}{
	GetEnumeration("Cash", "Method to pay in cash on delivery").ConvertToPaymentMethod(),
	GetEnumeration("CreditCardOnDelivery", "Method to pay with credit card on delivery").ConvertToPaymentMethod(),
	GetEnumeration("TicketCard", "Method to pay with ticket card on delivery").ConvertToPaymentMethod(),
	GetEnumeration("SodexoCard", "Method to pay with sodexo card on delivery").ConvertToPaymentMethod(),
}
