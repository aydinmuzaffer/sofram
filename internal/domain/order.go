package domain

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type Order struct {
	BaseEntitiy
	UserId                uuid.UUID
	Notes                 string
	VendorPaymentMethodId uuid.UUID
	OrderStatusId         uuid.UUID
	EstimatedDeliveryTime time.Time
	AddressId             uuid.UUID
	Total                 float64
}

func (Order) TableName() string {
	return "order"
}
