package db

import (
	"context"
	"errors"
	"fmt"
	"os"

	"github.com/golang-migrate/migrate/v4"
	postgresMigrate "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"

	//"github.com/jinzhu/gorm"
	//_ "github.com/jinzhu/gorm/dialects/postgres"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	log "github.com/sirupsen/logrus"
)

type SoframConnection struct {
	Db *gorm.DB
}

// NewSoframConnection NewDatabase - returns a pointer to a database object
func NewSoframConnection() (*SoframConnection, error) {
	log.Info("Setting up new database connection")

	dsn := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("SSL_MODE"),
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Info("connection refused for ", dsn)
		return &SoframConnection{}, fmt.Errorf("could not connect to database: %w", err)
	}

	return &SoframConnection{
		Db: db,
	}, nil
}

func (sc *SoframConnection) Close() {
	sqlDB, err := sc.Db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	sqlDB.Close()
}

func (sc *SoframConnection) Ping(ctx context.Context) error {
	db, err := sc.Db.DB()
	if err != nil {
		return err
	}

	if err := db.PingContext(ctx); err != nil {
		return err
	}

	return nil
}

// MigrateDB - runs all migrations in the migrations
func (sc *SoframConnection) MigrateDB() error {
	log.Info("migrating our database")

	db, err := sc.Db.DB()
	if err != nil {
		return err
	}

	driver, err := postgresMigrate.WithInstance(db, &postgresMigrate.Config{})
	if err != nil {
		return fmt.Errorf("could not create the postgres driver: %w", err)
	}

	m, err := migrate.NewWithDatabaseInstance(
		"file:///migrations",
		"postgres",
		driver,
	)
	if err != nil {
		log.Error(err)
		return err
	}

	if err := m.Up(); err != nil {
		if !errors.Is(err, migrate.ErrNoChange) {
			return fmt.Errorf("could not run up migrations: %w", err)
		}
	}

	log.Info("successfully migrated the database")
	return nil
}
